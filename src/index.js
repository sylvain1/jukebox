import { render } from 'react-dom'
import { persistStore } from 'redux-persist-immutable'
import configureStore from 'store'
// import registerServiceWorker from 'registerServiceWorker'
import 'css/all.css'
import App from 'App'

const store = configureStore()

persistStore(store, { whitelist: ['playlist', 'player'] }, () =>
  render(App({ store }), document.getElementById('root'))
)

// registerServiceWorker()
