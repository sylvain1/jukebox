import React from 'react'
import Jukebox from 'containers/Jukebox'
import { Provider } from 'react-redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

function App({ store }) {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={Jukebox} />
          </Switch>
        </main>
      </BrowserRouter>
    </Provider>
  )
}

export default App
