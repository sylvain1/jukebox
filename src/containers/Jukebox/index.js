import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import Player from 'containers/Player'
import Playlist from 'containers/Playlist'
import PropTypes from 'prop-types'
import { selectSongs } from 'containers/Playlist/selectors'

class Jukebox extends Component {
  render() {
    return (
      <div className="jukebox">
        <Player {...this.props} />
        <Playlist {...this.props} />
      </div>
    )
  }
}
Jukebox.propTypes = {
  songs: PropTypes.object,
}

const mapStateToProps = createSelector(selectSongs(), songs => ({
  songs,
}))

const mapDispatchToProps = dispatch => ({
  dispatch,
  // setPosition: position => dispatch(setPosition(position)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Jukebox)
