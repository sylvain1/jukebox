import { createSelector } from 'reselect'

const selectPlaylist = () => state => state.get('playlist')

const selectSongs = () =>
  createSelector(selectPlaylist(), substate => substate.get('songs').toJS())

const selectSongsMeta = () =>
  createSelector(selectPlaylist(), substate => substate.get('songsMeta').toJS())

const selectIsFetching = () =>
  createSelector(selectPlaylist(), substate => substate.get('isFetching'))

export { selectPlaylist, selectSongs, selectSongsMeta, selectIsFetching }
