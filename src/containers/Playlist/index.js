import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import PropTypes from 'prop-types'
import { fetchPlaylist } from './actions'
import { selectIsFetching } from './selectors'
import { selectSongId, selectState } from 'containers/Player/selectors'
import Song from 'containers/Song'

class Playlist extends Component {
  componentWillMount() {
    this.props.fetchPlaylist()
  }

  render() {
    if (this.props.isFetching) return <div>Loading...</div>

    const songs = this.props.songs
    return songs
      ? <div className="playlist">
          {Object.keys(songs).map(key => {
            return (
              <Song key={key} song={songs[key]} songId={this.props.songId} />
            )
          })}
        </div>
      : null
  }
}

Playlist.propTypes = {
  songId: PropTypes.number,
  songs: PropTypes.object,
  isFetching: PropTypes.bool,
  fetchPlaylist: PropTypes.func,
}

const mapStateToProps = createSelector(
  selectSongId(),
  selectState(),
  selectIsFetching(),
  (songId, state, isFetching) => ({
    songId,
    state,
    isFetching,
  })
)

const mapDispatchToProps = dispatch => ({
  dispatch,
  fetchPlaylist: () => dispatch(fetchPlaylist()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Playlist)
