import {
  FETCH_PLAYLIST,
  FETCH_PLAYLIST_SUCCESS,
  FETCH_PLAYLIST_FAILED,
} from './constants'

export function fetchPlaylist() {
  return {
    type: FETCH_PLAYLIST,
  }
}

export function fetchPlaylistSuccess(songs, songsMeta) {
  return {
    songs,
    songsMeta,
    type: FETCH_PLAYLIST_SUCCESS,
  }
}

export function fetchPlaylistFailed(error) {
  return {
    error,
    type: FETCH_PLAYLIST_FAILED,
  }
}
