import { takeLatest, call, put, select } from 'redux-saga/effects'
import request from 'utils/request'
import { fetchPlaylistSuccess, fetchPlaylistFailed } from './actions'
import { FETCH_PLAYLIST } from './constants'
import { selectSongs } from './selectors'
import { selectSongsMeta } from './selectors'

function* watchFetchPlaylistSaga() {
  yield takeLatest(FETCH_PLAYLIST, fetchPlaylistSaga)
}

function* fetchPlaylistSaga({ offset }) {
  const songsState = yield select(selectSongs())
  const songsMetaState = yield select(selectSongsMeta())

  const url = `/playlist.json`

  const { data } = yield call(request, url)

  if (data) {
    const songs = {}
    const songsMeta = {}

    data.forEach(song => {
      songs[song.id] = song
      songsMeta[song.id] = songsMetaState[song.id]
        ? songsMetaState[song.id]
        : {
            position: 0,
            duration: 0,
            state: 'stopped',
          }
    })

    const mergedSongs = { ...songsState, ...songs }

    yield put(fetchPlaylistSuccess(mergedSongs, songsMeta))
  } else {
    yield put(fetchPlaylistFailed('NO_RESULT'))
  }
}

export default [watchFetchPlaylistSaga]
