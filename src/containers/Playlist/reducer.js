import { fromJS } from 'immutable'
import {
  FETCH_PLAYLIST,
  FETCH_PLAYLIST_SUCCESS,
  FETCH_PLAYLIST_FAILED,
} from './constants'
import {
  SET_STATE,
  SET_POSITION,
  SET_DURATION,
  SET_SONG,
} from 'containers/Player/constants'

const initialState = fromJS({
  songs: {},
  songsMeta: {},
  isFetching: false,
})

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_PLAYLIST: {
      return state.merge({ isFetching: true })
    }
    case FETCH_PLAYLIST_SUCCESS: {
      return state.merge({
        songs: action.songs,
        songsMeta: action.songsMeta,
        isFetching: false,
      })
    }
    case FETCH_PLAYLIST_FAILED: {
      return state.merge({ isFetching: false })
    }
    case SET_POSITION: {
      const songsMeta = state.get('songsMeta').toJS()

      songsMeta[action.songId].position = action.position
      return state.merge({ songsMeta })
    }
    case SET_DURATION: {
      const songsMeta = state.get('songsMeta').toJS()

      songsMeta[action.songId].duration = action.duration
      return state.merge({ songsMeta })
    }
    case SET_STATE: {
      const songsMeta = state.get('songsMeta').toJS()

      songsMeta[action.songId].state = action.state
      return state.merge({ songsMeta })
    }
    case SET_SONG: {
      const songsMeta = state.get('songsMeta').toJS()
      for (const songMeta in songsMeta) {
        if (parseInt(songMeta, 10) !== action.songId) {
          songsMeta[songMeta].state = 'stopped'
        } else {
          songsMeta[songMeta].state = 'playing'
        }
      }
      return state.merge({ songsMeta })
    }

    default: {
      return state
    }
  }
}
