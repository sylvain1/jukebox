import { fromJS } from 'immutable'
import {
  SET_SONG,
  SET_STATE,
  SET_DURATION,
  SET_POSITION,
  SEEKED,
} from './constants'

const initialState = fromJS({
  songId: 0,
  state: '',
  duration: 0,
  position: 0,
  seek: false,
})

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_SONG: {
      return state.merge({
        songId: parseInt(action.songId, 10),
        state: 'playing',
      })
    }
    case SET_STATE: {
      return state.merge({ state: action.state })
    }
    case SET_DURATION: {
      return state.merge({ duration: action.duration })
    }
    case SET_POSITION: {
      return state.merge({ position: action.position, seek: action.seek })
    }
    case SEEKED: {
      return state.merge({ seek: false })
    }
    default: {
      return state
    }
  }
}
