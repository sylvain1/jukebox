import {
  SET_SONG,
  SET_STATE /* play / pause */,
  SET_DURATION,
  SET_POSITION,
  SEEKED,
} from './constants'

export function setSong(songId) {
  return {
    songId,
    type: SET_SONG,
  }
}
export function setState(state, songId) {
  return {
    state,
    songId,
    type: SET_STATE,
  }
}
export function setDuration(duration, songId) {
  return {
    duration,
    songId,
    type: SET_DURATION,
  }
}
export function setPosition(position, songId, seek = false) {
  return {
    position,
    songId,
    seek,
    type: SET_POSITION,
  }
}
export function seeked() {
  return {
    type: SEEKED,
  }
}
