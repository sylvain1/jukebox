import React, { Component } from 'react'
import formatDuration from 'utils/formatDuration'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import PropTypes from 'prop-types'
import {
  setSong,
  setState,
  setDuration,
  setPosition,
  seeked,
} from 'containers/Player/actions'
import {
  selectSongId,
  selectState,
  selectPosition,
  selectDuration,
  selectSeek,
} from 'containers/Player/selectors'
import PlayerControls from 'components/Player/PlayerControls'

class Player extends Component {
  state = {
    player: null,
    loading: false,
  }

  componentDidMount() {
    if (this.props.songId) {
      this.init()
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.songId === prevProps.songId &&
      this.props.state !== prevProps.state &&
      this.props.state === 'playing'
    ) {
      this.play()
    } else if (
      this.props.songId === prevProps.songId &&
      this.props.position !== prevProps.position &&
      this.props.seek !== prevProps.seek &&
      this.props.seek === true
    ) {
      this.seekTo(this.props.position)
    } else if (this.props.songId !== prevProps.songId) {
      this.init()
    }
  }

  init = () => {
    this.setState({ loading: true })

    const player = window.Mixcloud.PlayerWidget(
      document.querySelector('#mixcloud-player')
    )

    if (this.state.player) {
      this.state.player.events.progress.off()
      this.state.player.events.ended.off()
    }

    player.ready.then(() => {
      this.setState({ player, loading: false }, () => {
        if(this.props.position) {
          this.state.player.seek(this.props.position)
        }
        this.play()

        this.state.player.events.progress.on(this.progress)
        this.state.player.events.ended.on(this.ended)
      })
    })
  }

  progress = (position, duration) => {
    if (!this.state.player) return
    if (!position && !duration) return

    this.props.setPosition(position, this.props.songId)

    if (duration !== this.props.duration || !this.props.duration) {
      this.props.setDuration(duration, this.props.songId)
    }
  }

  ended = () => {
    this.next()
  }

  play = () => {
    if (this.state.player) {
      this.state.player.play()
      this.props.setState('playing', this.props.songId)
    }
  }

  pause = () => {
    if (this.state.player) {
      this.state.player.pause()
      this.props.setState('paused', this.props.songId)
    }
  }

  toggle = () => {
    if (this.props.state !== 'playing') {
      this.play()
    } else {
      this.pause()
    }
  }

  seekTo = position => {
    if (this.state.player) {
      this.state.player.seek(position)
      this.props.seeked()
    }
  }

  next = () => {
    let nextSongId
    const songsKeys = Object.keys(this.props.songs)
    const songIdIndex = songsKeys.indexOf(this.props.songId.toString())
    const nextIndex = songIdIndex + 1

    if (this.props.songs[songsKeys[nextIndex]]) {
      nextSongId = songsKeys[nextIndex]
    } else {
      nextSongId = songsKeys[0]
    }

    this.props.setSong(nextSongId)
  }
  prev = () => {
    let prevSongId
    const songsKeys = Object.keys(this.props.songs)
    const songIdIndex = songsKeys.indexOf(this.props.songId.toString())
    const prevIndex = songIdIndex - 1

    if (this.props.songs[songsKeys[prevIndex]]) {
      prevSongId = songsKeys[prevIndex]
    } else {
      prevSongId = songsKeys[songsKeys.length - 1]
    }

    this.props.setSong(prevSongId)
  }

  render() {
    const songId = this.props.songId
    if (!songId) return null

    const song = this.props.songs[songId]

    let position = song['position'] ? song.position : this.props.position
    let duration = song['duration'] ? song.duration : this.props.duration

    const url = encodeURIComponent(song.url)

    const iframe = `<iframe id="mixcloud-player" src="https://www.mixcloud.com/widget/iframe/?feed=${url}&amp;hide_cover=1&amp;light=1&amp;autoplay=0" frameBorder="0"></iframe>`

    const positionFormated = formatDuration(position * 1000)
    const durationFormated = formatDuration(duration * 1000)

    const progress = (position / duration * 100).toFixed(2)

    return (
      <div className="player">
        <div dangerouslySetInnerHTML={{ __html: iframe }} />

        <PlayerControls
          isLoading={this.state.loading}
          url={song.url}
          state={this.props.state}
          position={positionFormated}
          duration={durationFormated}
          progress={progress}
          play={this.play}
          pause={this.pause}
          toggle={this.toggle}
          next={this.next}
          prev={this.prev}
        />
      </div>
    )
  }
}

Player.propTypes = {
  songs: PropTypes.object,
  songId: PropTypes.number,
  state: PropTypes.string,
  position: PropTypes.number,
  duration: PropTypes.number,
  setSong: PropTypes.func,
  setState: PropTypes.func,
  setPosition: PropTypes.func,
  setDuration: PropTypes.func,
  seek: PropTypes.bool,
}

const mapStateToProps = createSelector(
  selectSongId(),
  selectState(),
  selectPosition(),
  selectDuration(),
  selectSeek(),
  (songId, state, position, duration, seek) => ({
    songId,
    state,
    position,
    duration,
    seek,
  })
)

const mapDispatchToProps = dispatch => ({
  dispatch,
  setSong: songId => dispatch(setSong(songId)),
  setState: (state, songId) => dispatch(setState(state, songId)),
  setPosition: (position, songId) => dispatch(setPosition(position, songId)),
  setDuration: (duration, songId) => dispatch(setDuration(duration, songId)),
  seeked: () => dispatch(seeked()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Player)
