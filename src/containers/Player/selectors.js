import { createSelector } from 'reselect'

const selectPlayer = () => state => state.get('player')

const selectSongId = () =>
  createSelector(selectPlayer(), substate => substate.get('songId'))

const selectState = () =>
  createSelector(selectPlayer(), substate => substate.get('state'))

const selectDuration = () =>
  createSelector(selectPlayer(), substate => substate.get('duration'))

const selectPosition = () =>
  createSelector(selectPlayer(), substate => substate.get('position'))

const selectSeek = () =>
  createSelector(selectPlayer(), substate => substate.get('seek'))

export { selectSongId, selectState, selectDuration, selectPosition, selectSeek }
