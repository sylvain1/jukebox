import React, { Component } from 'react'
import { connect } from 'react-redux'
import { createSelector } from 'reselect'
import PropTypes from 'prop-types'
import { setSong, setPosition } from 'containers/Player/actions'
import classnames from 'classnames'
import { selectSongsMeta } from 'containers/Playlist/selectors'
import SongItem from 'components/Song/SongItem'

class Song extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    const songMeta = this.props.songsMeta[this.props.song.id]
    const nextSongMeta = nextProps.songsMeta[nextProps.song.id]

    if (
      songMeta.position !== nextSongMeta.position ||
      songMeta.duration !== nextSongMeta.duration ||
      songMeta.state !== nextSongMeta.state
    ) {
      return true
    }
    return false
  }

  handleOnClick = ({ nativeEvent }) => {
    if (
      this.props.songId !== this.props.song.id ||
      this.props.songsMeta[this.props.song.id].state !== 'playing'
    ) {
      this.props.setSong(this.props.song.id)
    } else {
      const clickPosition = nativeEvent.layerX
      const songWidth = nativeEvent.target.clientWidth
      const ratio = clickPosition / songWidth

      this.props.setPosition(
        ratio * this.props.songsMeta[this.props.song.id]['duration'],
        this.props.songId,
        true
      )
    }
  }

  render() {
    const { song, songsMeta } = this.props

    if (!song) return

    const songMeta = songsMeta[song.id]

    let progress = 0
    if (songMeta && songMeta['position'] && songMeta['duration']) {
      progress = songMeta.position / songMeta.duration * 100
    }

    const css = classnames('song', {
      [songMeta.state]: true,
    })

    return (
      <SongItem
        className={css}
        setSong={this.handleOnClick}
        progress={progress}
        song={song}
        songMeta={songMeta}
      />
    )
  }
}

Song.propTypes = {
  song: PropTypes.object,
  songId: PropTypes.number,
  songsMeta: PropTypes.object,
  setSong: PropTypes.func,
}

const mapStateToProps = createSelector(selectSongsMeta(), songsMeta => ({
  songsMeta,
}))

const mapDispatchToProps = dispatch => ({
  dispatch,
  setSong: song => dispatch(setSong(song)),
  setPosition: (position, songId, seek) =>
    dispatch(setPosition(position, songId, seek)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Song)
