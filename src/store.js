import { createStore, applyMiddleware, compose } from 'redux'
import { fromJS } from 'immutable'
import createSagaMiddleware from 'redux-saga'
import playlistSagas from 'containers/Playlist/sagas'
import { autoRehydrate } from 'redux-persist-immutable'
import reducers from 'reducers'

const sagaMiddleware = createSagaMiddleware()

export default function configureStore() {
  const middlewares = [sagaMiddleware]

  const enhancers = [applyMiddleware(...middlewares), autoRehydrate()]

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      : compose

  const store = createStore(
    reducers,
    fromJS({}),
    composeEnhancers(...enhancers)
  )

  // Saga
  store.runSaga = sagaMiddleware.run

  // Map global sagas
  playlistSagas.map(store.runSaga)

  return store
}
