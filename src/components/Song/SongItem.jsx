import React from 'react'
import PropTypes from 'prop-types'
import formatDuration from 'utils/formatDuration'

function SongItem({ className, setSong, song, progress, songMeta }) {
  const positionFormated = formatDuration(songMeta['position'] * 1000)
  const durationFormated = formatDuration(songMeta['duration'] * 1000)

  const Position =
    songMeta['duration'] !== 0 &&
    <div className="position">
      {positionFormated} / {durationFormated}
    </div>

  const handleClick = event => {
    event.preventDefault()
    setSong(event)
  }

  return (
    <div className={className}>
      <a onClick={handleClick}>
        {song.title}
      </a>
      <div className="progress" style={{ width: `${progress.toFixed(2)}%` }} />
      {Position}
    </div>
  )
}

SongItem.propTypes = {
  className: PropTypes.string,
  song: PropTypes.object,
  songMeta: PropTypes.object,
  progress: PropTypes.number,
  setSong: PropTypes.func,
}

export default SongItem
