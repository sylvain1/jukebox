import { combineReducers } from 'redux-immutable'
import playlist from 'containers/Playlist/reducer'
import player from 'containers/Player/reducer'

const reducers = combineReducers({
  playlist,
  player,
})

export default reducers
