import parseMs from 'parse-ms'
import addZero from 'add-zero'

export default function formatDuration(ms) {
  let { days, hours, minutes, seconds } = parseMs(ms)
  seconds = addZero(seconds)
  if (days) return `${days}:${addZero(hours)}:${addZero(minutes)}:${seconds}`
  if (hours) return `${hours}:${addZero(minutes)}:${seconds}`
  return `${minutes}:${seconds}`
}
